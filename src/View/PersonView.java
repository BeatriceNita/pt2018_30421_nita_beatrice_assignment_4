package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BankManagement.Bank;
import BankManagement.Person;
import Table.Table;
import Table.TableModel;
import Table.InfoTable;
import Table.InfoTableModel;

public class PersonView implements ActionListener{

	private JFrame frame = new JFrame("Person Processing");
	
	private JPanel info = new JPanel();
	private JPanel actions = new JPanel();
	private JPanel table = new JPanel();
	private JPanel infoTablePanel = new JPanel();

	private JLabel nameLabel = new JLabel("Introduce Name:");
	
	private JTextField nameText = new JTextField();
	
	private String name;
	private Bank bank;
	
	TableModel tableModel = new TableModel();
	Table personstable;
	
	private JButton addPerson = new JButton("Add Person");
	//private JButton editPerson = new JButton("Edit Person");
	private JButton removePerson = new JButton("Remove Person");
	//private JButton viewAllPersons = new JButton("View All Persons");
	
	public PersonView(Bank bank) {
		this.bank = bank;
		
		frame.setSize(800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		info.setLayout(new GridLayout(1, 2));
		info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		info.setVisible(true);

		info.add(nameLabel);
		info.add(nameText);
		
		nameText.addActionListener(this);
		
		actions.setLayout(new FlowLayout());
		
		actions.add(addPerson);
		//actions.add(editPerson);
		actions.add(removePerson);
		//actions.add(viewAllPersons);
		
		addPerson.setActionCommand("add");
		//editPerson.setActionCommand("edit");
		removePerson.setActionCommand("remove");
		//viewAllPersons.setActionCommand("view");
		
		addPerson.setBackground(Color.yellow);
		//editPerson.setBackground(Color.yellow);
		removePerson.setBackground(Color.yellow);
		//viewAllPersons.setBackground(Color.yellow);
		
		addPerson.addActionListener(this);
		//editPerson.addActionListener(this);
		removePerson.addActionListener(this);
		//viewAllPersons.addActionListener(this);
		
		table.setLayout(new FlowLayout());
		
		table.setVisible(false);
		infoTablePanel.setVisible(false);
		
		frame.add(info, BorderLayout.NORTH);
		frame.add(actions, BorderLayout.CENTER);
		frame.add(table, BorderLayout.SOUTH);
		frame.add(infoTablePanel, BorderLayout.SOUTH);
	}
	
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("add")) {
			
			Random randomGenerator = new Random();

			name = nameText.getText();
			Person newClient = new Person(randomGenerator.nextInt(100), name);
			bank.addPerson(newClient);
			updateClients();
			bank.serialize();
		}
		/*else if (e.getActionCommand().equals("edit")) {
			//ID = Integer.parseInt(idText.getText());
			name = nameText.getText();
		}*/
		else if (e.getActionCommand().equals("remove")) {
			name = nameText.getText();
			
			boolean found = false;
			Person foundPerson = null;

			for (Person client : bank.getClients()) {
				if (client.getName().equals(name)) {
					found = true;
					foundPerson = client;
				}
			}
			if (found) {
				bank.removePerson(foundPerson);
				updateClients();
			}

			bank.serialize();
		}
		/*else if (e.getActionCommand().equals("view")) {
			updateClients();
		}*/
	}
	
	public  void updateClients() {
		for (int j = tableModel.getRowCount() - 1; j >= 0; j--) {
			tableModel.removeClient(j);
		}
		ArrayList<Person> clients;
		clients = bank.getClients();
		for (Person p : clients) {
			tableModel.addCustomer(p);
		}
		personstable = new Table(tableModel);
		table.add(personstable);
		table.setVisible(true);
		infoTablePanel.setVisible(false);
		//table.setSize(850, 400);
		//setVisible(true);
		frame.add(table, BorderLayout.SOUTH);
	}
}
