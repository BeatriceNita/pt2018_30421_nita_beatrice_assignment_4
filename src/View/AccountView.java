package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import BankManagement.Account;
import BankManagement.Bank;
import BankManagement.Person;
import BankManagement.SavingAccount;
import BankManagement.SpendingAccount;
import Table.Table;
import Table.TableModel;
import Table.InfoTable;
import Table.InfoTableModel;

public class AccountView implements ActionListener{

	private JFrame frame = new JFrame("Accounts Processing");
	
	private JPanel info = new JPanel();
	private JPanel actions1 = new JPanel();
	private JPanel actions2 = new JPanel();
	private JPanel table = new JPanel();
	private JPanel infoTablePanel = new JPanel();
	
	private JLabel nameLabel = new JLabel("Introduce Name:");
	private JLabel idLabel1 = new JLabel("Introduce Account ID:");
	private JLabel pinLabel = new JLabel("Introduce PIN:");
	private JLabel typeLabel = new JLabel("Introduce the type of Account:");
	private JLabel sumLabel = new JLabel("Introduce Sum:");
	
	private JTextField nameText = new JTextField();
	private JTextField accIdText = new JTextField();
	private JTextField pinText = new JTextField();
	private JTextField typeText = new JTextField();
	private JTextField sumText = new JTextField();
	
	private int AccID;
	private String name;
	private String type;
	private String PIN;
	private double sum;
	
	private Bank bank;
	
	InfoTableModel infoTableModel = new InfoTableModel();
	InfoTable infoTable;
	
	private JButton addAccount = new JButton("Add Account");
	private JButton removeAccount = new JButton("Remove Account");
	//private JButton viewAllAccounts = new JButton("View All Accounts");
	
	private JButton deposit = new JButton("Deposit");
	private JButton withdraw = new JButton("Withdraw");

	public AccountView(Bank bank) {
		this.bank = bank;
		
		frame.setSize(800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		info.setLayout(new GridLayout(5, 2));
		info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		info.setVisible(true);

		info.add(nameLabel);
		info.add(nameText);
		info.add(idLabel1);
		info.add(accIdText);
		info.add(pinLabel);
		info.add(pinText);
		info.add(typeLabel);
		info.add(typeText);
		info.add(sumLabel);
		info.add(sumText);
		
		nameText.addActionListener(this);
		accIdText.addActionListener(this);
		pinText.addActionListener(this);
		typeText.addActionListener(this);
		sumText.addActionListener(this);
		
		actions1.setLayout(new FlowLayout());
		
		actions1.add(addAccount);
		actions1.add(removeAccount);
		//actions1.add(viewAllAccounts);
		
		addAccount.setActionCommand("addAcc");
		removeAccount.setActionCommand("removeAcc");
		//viewAllAccounts.setActionCommand("viewAcc");
		
		addAccount.setBackground(Color.WHITE);
		removeAccount.setBackground(Color.WHITE);
		//viewAllAccounts.setBackground(Color.WHITE);
		
		addAccount.addActionListener(this);
		removeAccount.addActionListener(this);
		//viewAllAccounts.addActionListener(this);
		
		actions2.setLayout(new FlowLayout());
		
		actions2.add(deposit);
		actions2.add(withdraw);
		
		deposit.setActionCommand("deposit");
		withdraw.setActionCommand("withdraw");
		
		deposit.setBackground(Color.CYAN);
		withdraw.setBackground(Color.CYAN);
		
		deposit.addActionListener(this);
		withdraw.addActionListener(this);
		
		table.setVisible(false);
		infoTablePanel.setVisible(false);
		
		frame.add(info, BorderLayout.NORTH);
		frame.add(actions1, BorderLayout.WEST);
		frame.add(actions2, BorderLayout.EAST);
		frame.add(table, BorderLayout.SOUTH);
		frame.add(infoTablePanel, BorderLayout.SOUTH);
	}
	
	public  void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("addAcc")) {
			//AccID = Integer.parseInt(accIdText.getText());
			name = nameText.getText();
			type = typeText.getText();
			PIN = pinText.getText();
			//sum = Double.parseDouble(sumText.getText());
			
			Person foundPerson = null;
			Random randomGenerator = new Random();
			boolean found = false;
			
			for (Person p : bank.getClients()) {
				if (p.getName().equals(name)) {
					found = true;
					foundPerson = p;
				}
			}

			if (!found) {
				Person newClient = new Person(randomGenerator.nextInt(100), name);
				bank.addPerson(newClient);
				bank.addHolderAccount(newClient, type, PIN);
				updateInfo(newClient);
			} else {
				bank.addHolderAccount(foundPerson, type, PIN);
				updateInfo(foundPerson);
			}
			bank.serialize();
			
			
		}
		else if (e.getActionCommand().equals("removeAcc")) {
			AccID = Integer.parseInt(accIdText.getText());
			name = nameText.getText();
			
			
			Person person = null;
			Account account = null;

			for (Person client : bank.getClients()) {
				if (client.getName().equals(name)) {
					for (Account a : bank.getAccounts()) {
						if (a.getOwnerName().equals(client.getName())) {
							account = a;
							person = client;
						}
					}
				}
			}
			bank.removeHolderAccount(person, account);
			updateInfo(person);
			bank.serialize();
		}
		/*else if (e.getActionCommand().equals("viewAcc")) {
			//AccID = Integer.parseInt(accIdText.getText());
			name = nameText.getText();
		}*/
		else if (e.getActionCommand().equals("deposit")) {
			//name = nameText.getText();
			PIN = pinText.getText();
			sum = Double.parseDouble(sumText.getText());
			
			Person person = null;
			Account account = null;
	
			for (Person client : bank.getClients()) {
				if (client.getName().equals(name)) {
					for (Account a : bank.getAccounts()) {
						if (a.getOwnerName().equals(client.getName())) {
							account = a;
							person = client;
							account.deposit(person, account, sum);
							bank.update(account, bank);
						}
					}
				}
			}
			System.out.println(bank.getObserverReport());
			bank.serialize();
		}
		else if (e.getActionCommand().equals("withdraw")) {
			//name = nameText.getText();
			PIN = pinText.getText();
			sum = Double.parseDouble(sumText.getText());
			
			Person person = null;
			Account account = null;

			for (Person client : bank.getClients()) {
				if (client.getName().equals(name)) {
					for (Account a : bank.getAccounts()) {
						if (a.getOwnerName().equals(client.getName())) {
							account = a;
							person = client;
							if (!account.cannotWithdraw(sum)) {
								JOptionPane.showMessageDialog(null, "The total of the account is too small for this operation");
							} else {
								account.withdraw(person, account, sum);
							}
							bank.update(account, bank);
						}
					}
				}
			}
			System.out.println(bank.getObserverReport());
			bank.serialize();
		}
	}
	
	
	public  void updateInfo(Person client) {
		for (int j = infoTableModel.getRowCount() - 1; j >= 0; j--) {
			infoTableModel.removeAccount(j);
		}
		ArrayList<Account> accounts = new ArrayList<Account>();
		accounts = bank.getAccounts();
		if (bank.getHashtable().get(client) != null)
			for (Iterator<Account> iterator = bank.getHashtable().get(client).iterator(); iterator.hasNext();) {
				Account value = iterator.next();
				if (value != null && value.getOwnerName().equals(client.getName())) {
					infoTableModel.addAccount(value);
				}

			}
		infoTable = new InfoTable(infoTableModel);
		infoTablePanel.add(infoTable);
		infoTablePanel.setVisible(true);
		table.setVisible(false);
		//infoTablePanel.setSize(850, 450);
		
		frame.add(infoTablePanel, BorderLayout.SOUTH);
	}
}
