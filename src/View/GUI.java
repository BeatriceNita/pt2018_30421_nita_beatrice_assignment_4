package View;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import BankManagement.Bank;
import View.AccountView;
import View.PersonView;
import BankManagement.Account;
import View.AccountView;

public class GUI implements ActionListener{

	private JFrame frame = new JFrame();
	private JPanel panel = new JPanel();
	private JPanel mainPanel = new JPanel();
	//private JPanel loginPanel = new JPanel();
	
	private JButton AdminProc;
	private JButton PersonProc;
	//private JButton ClientProc;
	
	private Bank bank;
	
	public GUI(Bank bank) {
		this.bank = bank;
		
		frame.setTitle("Bank Management");//the title of the frame
		frame.setVisible(true);//very important! we want to see all the components of our frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//this is meant for when the close button is pushed, the frame to close
		frame.setSize(800, 300);//the size of the frame
		frame.setLocationRelativeTo(null);//it places the cursor at the center of the frame
		
		AdminProc = new JButton("Account Ops");
		PersonProc = new JButton("Person Ops");
		//ClientProc = new JButton("Client Ops");
		
		AdminProc.setActionCommand("process admin");
		PersonProc.setActionCommand("process person");
		//ClientProc.setActionCommand("process client");
		
		AdminProc.addActionListener(this);
		//ClientProc.addActionListener(this);
		PersonProc.addActionListener(this);
		
		AdminProc.setBackground(Color.WHITE);
		AdminProc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		PersonProc.setBackground(Color.yellow);
		PersonProc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		//ClientProc.setBackground(Color.CYAN);
		//ClientProc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		panel.setLayout(new GridLayout(1,2));
		panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		panel.setVisible(true);
		
		panel.add(AdminProc);
		panel.add(PersonProc);
		//panel.add(ClientProc);
		
		frame.add(panel);
	}
	
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("process admin")) {
			mainPanel.setVisible(false);
			AccountView accountView = new AccountView(bank);
		}
		else if (e.getActionCommand().equals("process person")) {
			PersonView p = new PersonView(bank);
		}
		/*else if (e.getActionCommand().equals("process client")) {
			//ClientView c = new ClientView(bank);
			Account foundAccount = null;
			boolean found = false;
			
			String pin = JOptionPane.showInputDialog("Enter your PIN");
			String PIN = pin.trim();
			
				for (Account a : bank.getAccounts()) {
					if (PIN.equals(a.getPIN())) {
					foundAccount = a;
					found = true;
					}
				}
				if (found) {
					//frame.setSize(800, 800);
					loginPanel.setVisible(false);
					ClientView c = new ClientView(bank, foundAccount);
				}
			}*/
	}
	
}
