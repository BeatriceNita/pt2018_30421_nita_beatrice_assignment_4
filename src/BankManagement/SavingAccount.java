package BankManagement;

public class SavingAccount extends Account{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SavingAccount(int ID, String ownerName, double total, String PIN) {
		super(ID, ownerName, total, PIN);
		//super.setTotal(30000);
	}

	@Override
	public  void deposit(Person p, Account a, double sum) {
		setTotal(getTotal() + sum);
		notifyObservers("Changed sum");
	}

	@Override
	public  void withdraw(Person p, Account a, double sum) {
		if (getTotal()>= sum){
			setTotal(getTotal()- sum);
			notifyObservers("Changed sum");
		}
		else {
			notifyObservers("Not enough funds");
		}
	}

	public  boolean cannotWithdraw(double sum) {
		if (getTotal() - sum < 0) {
			return false;
		} else {
			return true;
		}
	}


}
