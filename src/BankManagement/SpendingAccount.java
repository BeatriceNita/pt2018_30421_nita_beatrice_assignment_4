package BankManagement;

public class SpendingAccount extends Account{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpendingAccount(int ID, String ownerName, double total, String PIN) {
		super(ID, ownerName, total, PIN);
	}
	
	@Override
	public  void deposit(Person p, Account a, double sum) {
		setTotal(getTotal() + sum);
	}

	@Override
	public  void withdraw(Person p, Account a, double sum) {
		if (getTotal()>= sum){
			setTotal(getTotal()- sum);
		}
	}

	public  boolean cannotWithdraw(double sum) {
		if (getTotal() - sum < 0) {
			return false;
		} else {
			return true;
		}
	}

}

