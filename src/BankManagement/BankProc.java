package BankManagement;

import BankManagement.Person;

public interface BankProc {
	
	/**
	 * @pre p != null
	 * @post persons.size() = persons.size()@pre + 1
	 * @post get(p) != null
	 */
	
	public void addPerson(Person p);
	
	/**
	 * @pre p != null
	 * @pre containsKey(p)
	 * @post persons.size() = persons.size()@pre - 1
	 * @post !containsKey(p)
	 */
	
	public void removePerson(Person p);
	
	/**
	 * @pre p != null
	 * @pre type != null
	 * @pre PIN != 0
	 * @post accounts.size() = accounts.size()@pre + 1
	 */
	
	public Account addHolderAccount(Person p, String type, String PIN);
	
	/**
	 * @pre p != null
	 * @pre a != null
	 * @post accounts.size() = accounts.size()@pre - 1
	 */
	
	public void removeHolderAccount(Person p, Account a);
	
	/**
	 * @pre p != null
	 * @pre a != null
	 */
	
	public void readAccountData(Person p, Account a);
	
	/**
	 * 
	 * @pre p != null
	 * @pre a != null
	 * @pre data != 0
	 * @post a.getTotal() != a.getTotal()@pre
	 */
	
	public void writeAccountData(Person p, Account a, double data);
	
	public void generateReport();
}
