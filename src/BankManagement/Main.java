package BankManagement;

import View.GUI;

public class Main {

	public static void main(String[] args) {
		Bank bank = new Bank();

		bank.serialize();

		Bank newBank = new Bank();
		newBank = bank.deserialize();
		System.out.println(newBank);
		
		GUI gui = new GUI(newBank);
	}

}
