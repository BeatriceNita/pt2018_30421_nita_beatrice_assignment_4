package BankManagement;

import java.io.Serializable;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int ID;
	private String ownerName;
	private double total;
	private String PIN;
	
	public Account(int ID, String ownerName, double total, String PIN){
		this.ID = ID;
		this.ownerName = ownerName;
		this.total = total;
		this.PIN = PIN;
	}
	
	public int getID() {
		return ID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}
	
	public String getOwnerName() {
		return ownerName;
	}
	
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	} 
	
	public double getTotal() {
		return total;
	}
	
	public void setTotal(double total) {
		this.total = total;
	}
	
	public String getPIN() {
		return PIN;
	}
	
	public void setPIN(String PIN) {
		this.PIN = PIN;
	}
	
	public abstract void deposit(Person p, Account a, double sum);

	public abstract void withdraw(Person p, Account a, double sum);
	
	public String toString() {
		return String.format("Account ID: %d / Account Balance: %d / Account PIN: %s\n", ID, total, PIN);
	}
	
	public abstract  boolean cannotWithdraw(double sum);

}
