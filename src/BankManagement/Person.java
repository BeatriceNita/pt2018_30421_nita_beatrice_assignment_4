package BankManagement;

import java.io.Serializable;

public class Person implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int ID;
	private String Name;
	
	public Person(int ID, String Name) {
		this.ID = ID;
		this.Name = Name;
	}
	
	public int getID() {
		return ID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	} 
	
	public String getName() {
		return Name;
	}
	
	public void setName(String Name) {
		this.Name = Name;
	} 
	
	public String toString() {
		return String.format("ID:%d Name: %s\n", ID, Name);
	}
	
}
