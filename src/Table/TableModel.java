package Table;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import BankManagement.Person;

public class TableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String[] columnNames = { "ID", "Name" };
	private ArrayList<Person> clients;

	public  TableModel() {
		clients = new ArrayList<Person>();
	}

	public  TableModel(ArrayList<Person> clients) {
		this.clients = clients;
	}

	@Override
	public  int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public  int getRowCount() {
		return clients.size();
	}

	public  String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public  Object getValueAt(int row, int column) {
		Person client = getClient(row);
		switch (column) {
		case 0:
			return client.getID();
		case 1:
			return client.getName();
		}
		return client;
	}

	public  Person getClient(int row) {
		return clients.get(row);
	}

	public  void addCustomer(Person client) {
		insertClient(getRowCount(), client);
	}

	public  void insertClient(int row, Person client) {
		clients.add(row, client);
		fireTableRowsInserted(row, row);
	}

	public  void removeClient(int row) {
		clients.remove(row);
		fireTableRowsDeleted(row, row);
	}

}
